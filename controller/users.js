const User = require("../models/sequelize/user");
const MyError = require("../utils/myError");
const asyncHandler = require("express-async-handler");
const paginate = require("../utils/paginate");
const sendEmail = require("../utils/email");
const crypto = require("crypto");

exports.register = asyncHandler(async (req, res, next) => {
  const user = await req.db.user.findOne({ where: { email: req.body.email } });
  if (user) {
    throw new MyError("Имэйл хаяг бүртгэлтэй байна", 400);
  }
  const newUser = await req.db.user.create(req.body);
  res.status(200).json({
    success: true,
    token: newUser.getJsonWebToken(),
    user: newUser,
  });
});

exports.login = asyncHandler(async (req, res, next) => {
  //Оролтоо шалгана
  const { email, password } = req.body;
  if (!email || !password) {
    throw new MyError("Имэйл болон нууц үгээ дамжуулна уу", 400);
  }
  //Тухайн хэрэглэгчийг хайна
  const user = await req.db.user.findOne({ where: { email } });
  if (!user) {
    throw new MyError("Имэйл болон нууц үгээ зөв оруулна уу", 401);
  }
  const ok = await user.checkPassword(password);
  if (!ok) {
    throw new MyError("Имэйл болон нууц үгээ зөв оруулна уу", 401);
  }
  const token = user.getJsonWebToken();
  const cookieOption = {
    expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000),
    httpOnly: true,
  };
  res.status(200).cookie("amazon-token", token, cookieOption).json({
    success: true,
    user,
    token,
  });
});

exports.getUsers = asyncHandler(async (req, res, next) => {
  const page = parseInt(req.query.page) || 1;
  const limit = parseInt(req.query.limit) || 50;
  let select = req.query.select;
  const sort = req.query.sort;
  if (select) {
    select = select.split(" ");
  }
  ["select", "sort", "page", "limit"].forEach((el) => delete req.query[el]);
  const pagination = await paginate(page, limit, req.db.user);
  let query = { offset: pagination.start - 1, limit };

  if (req.query.where) {
    query.where = req.query.where;
  }
  if (select) {
    query.attributes = select;
  }
  if (sort) {
    query.order = sort
      .split(" ")
      .map((el) => [
        el.charAt(0) === "-" ? el.substring(1) : el,
        el.charAt(0) === "-" ? "DESC" : "ASC",
      ]);
  }
  console.log("query", query);
  let users = await req.db.user.findAll(query);
  res.status(200).json({
    success: true,
    data: users,
    pagination,
  });
});

exports.getUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);
  if (!user) {
    throw new MyError(req.params.id + " ID-тэй хэрэглэгч байхгүй.", 403);
  }
  res.status(200).json({
    success: true,
    data: user,
  });
});

exports.createUser = asyncHandler(async (req, res, next) => {
  console.log(req.body);
  const user = await User.create(req.body);
  res.status(200).json({
    success: true,
    data: user,
  });
});

exports.updateUser = asyncHandler(async (req, res, next) => {
  const user = await User.findByIdAndUpdate(req.params.id, req.body, {
    new: true,
    runValidators: true,
  });
  if (!user) {
    throw new MyError(req.params.id + " ID-тэй хэрэглэгч байхгүй.", 400);
  }

  res.status(200).json({
    success: true,
    data: user,
  });
});

exports.deleteUser = asyncHandler(async (req, res, next) => {
  const user = await User.findById(req.params.id);
  if (!user) {
    throw new MyError(req.params.id + " ID-тэй хэрэглэгч байхгүй.", 400);
  }
  user.remove();
  res.status(200).json({
    success: true,
    data: user,
  });
});

exports.forgotPassword = asyncHandler(async (req, res, next) => {
  if (!req.body.email) {
    throw new MyError("Та нууц үгээ сэргээх имэйл хаягаа дамжуулна уу", 400);
  }
  const user = await User.findOne({ email: req.body.email });
  if (!user) {
    throw new MyError(req.body.email + " email-тэй хэрэглэгч байхгүй.", 403);
  }
  const resetToken = user.generatePasswordChangeToken();
  //token hadlagah
  await user.save();
  // await user.save({validateBeforeSave : false});
  //Имэйл илгээнэ
  const link = `https://amazon.mn/changepassword/${resetToken}`;
  const message = `Сайн байна уу<br><br> Нууц үгээ солих хүсэлт илгээлээ.<br> Нууц үгээ доорхи линк дээр дарж солино уу:<br><br><a href="${link}">${link}</a><br><br>Өдрийг сайхан өнгөрүүлээрэй!`;
  const info = await sendEmail({
    email: user.email,
    subject: "Нууц үг өөрчлөх хүсэлт",
  });
  res.status(200).json({
    success: true,
    resetToken,
    message,
  });
});
exports.resetPassword = asyncHandler(async (req, res, next) => {
  if (!req.body.resetToken || !req.body.password) {
    throw new MyError("Та token болон нууц үгээ дамжуулна уу", 400);
  }

  const encrypted = crypto
    .createHash("sha256")
    .update(req.body.resetToken)
    .digest("hex");

  const user = await User.findOne({
    resetPasswordToken: encrypted,
    resetPasswordExpire: { $gt: Date.now() },
  });
  if (!user) {
    throw new MyError("Token хүчингүй байна", 400);
  }

  user.password = req.body.password;
  user.resetPasswordToken = undefined;
  user.resetPasswordExpire = undefined;

  await user.save();

  res.status(200).json({
    success: true,
    token: user.getJsonWebToken(),
  });
});

exports.logout = asyncHandler(async (req, res, next) => {
  const cookieOption = {
    expires: new Date(Date.now() - 30 * 24 * 60 * 60 * 1000),
    httpOnly: true,
  };
  res.status(200).cookie("amazon-token", null, cookieOption).json({
    success: true,
  });
});
