const jwt = require("jsonwebtoken");
const asyncHandler = require("./asyncHandler")
const MyError = require('../utils/myError')


exports.protect = asyncHandler(async (req, res, next) => {
    let token = null;

    if (req.headers.authorization) {
        token = req.headers.authorization.split(" ")[1];
    } else if (req.cookies) {
        console.log("cookie---", req.cookies);
        token = req.cookies["amazon-token"]
    }


    if (!token) {
        throw new MyError('Та token дамжуулна уу', 401)
    }
    const tokenObj = jwt.verify(token, process.env.JWT_SECRET);//token шалгана
    // req.user = await User.findById(tokenObj.id);
    req.userId = tokenObj.id;
    req.userRole = tokenObj.role;
    next();
})

exports.authorize = (...roles) => {
    return (req, res, next) => {
        req.userId
        if (!roles.includes(req.userRole)) {
            throw new MyError('Таны эрх [' + req.userRole + '] энэ үйлдийг гүйцэтгэхэд хүрэлцэхгүй байна', 403)
        }
        next();
    }
}