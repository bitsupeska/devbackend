const { set } = require("mongoose");
const MyError = require("../../utils/myError");

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('comment', {
    id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    userId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'user',
        key: 'id'
      }
    },
    bookId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: {
        model: 'book',
        key: 'id'
      }
    },
    comment: {
      type: DataTypes.STRING(450),
      allowNull: false,
      validate: {
        // customValidator(value) {
        //   console.log(this.userId);
        //   if (parseInt(this.userId) === 1) {
        //     throw new MyError("Ийм хэрэглэнч ком нэмж чадахгүй", 400)
        //   }
        // },
        isEmail: {
          msg: "Заавал имэйл оруулна уу"
        },
        // min: {
        //   args: [20],
        //   msg: 'Хамгийн багадаа 20 байх ёстой'
        // }
        notContains: {
          args: ['миа'],
          msg: "Энэ мессэжинд хориглогдсон үг байна"
        }
      },
      // get() {
      //   let comment = this.getDataValue('comment').toLowerCase()
      //   return comment.charAt(0).toUpperCase() + comment.substring(1);
      // },
      // set(value) {
      //   this.setDataValue("comment", value.replace('миа', 'тиймэрхүү'))
      // }
    }
  }, {
    sequelize,
    tableName: 'comment',
    timestamps: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "id" },
        ]
      },
      {
        name: "FK_comment_1_idx",
        using: "BTREE",
        fields: [
          { name: "userId" },
        ]
      },
      {
        name: "FK_comment_2_idx",
        using: "BTREE",
        fields: [
          { name: "bookId" },
        ]
      },
    ]
  });
};
