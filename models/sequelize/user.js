const bcrypt = require("bcrypt")
const jwt = require("jsonwebtoken")
const crypto = require("crypto")
module.exports = function (sequelize, DataTypes) {
  let User = sequelize.define(
    "user",
    {
      id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
      },
      name: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
      email: {
        type: DataTypes.STRING(200),
        allowNull: false,
      },
      role: {
        type: DataTypes.STRING(200),
        allowNull: true,
      },
      password: {
        allowNull: false,
        type: DataTypes.STRING(255),
      },
      about: {
        type: DataTypes.VIRTUAL,
        get() {
          return `${this.name} - ${this.email} - ${this.role}`;
        },
      },
    },
    {
      sequelize,
      tableName: "user",
      timestamps: false,
      indexes: [
        {
          name: "PRIMARY",
          unique: true,
          using: "BTREE",
          fields: [{ name: "id" }],
        },
      ],
    }
  );
  User.beforeCreate(async (user, options) => {
    const salt = await bcrypt.genSalt(10);
    user.password = await bcrypt.hash(user.password, salt);
  });

  User.prototype.checkPassword = async function (password) {
    console.log("password",password)
    console.log("password",this.password)
    return await bcrypt.compare(password, this.password);
  };
  User.prototype.generatePasswordChangeToken = function () {
    const resetToken = crypto.randomBytes(30).toString("hex");

    this.resetPasswordToken = crypto
      .createHash("sha256")
      .update(resetToken)
      .digest("hex");

    this.resetPasswordExpire =
      Date.now() + process.env.CHANGE_PASSWORD_EXPIRE_MIN * 60 * 1000;

    return resetToken;
  };
  User.prototype.generatePassword = async function (password) {
    const salt = await bcrypt.genSalt(10);
    return await bcrypt.hash(password, salt);
  };
  User.prototype.getJsonWebToken = function () {
    const token = jwt.sign(
      {
        id: this.dataValues.id,
        roleId: this.dataValues.roleId,
        orgId: this.dataValues.organizationId,
      },
      process.env.JWT_SECRET,
      {
        expiresIn: process.env.JWT_EXPIRESIN,
      }
    );
    return token;
  };
  return User;
};
