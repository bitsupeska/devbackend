const express = require("express");
const { createComment, updateComment, deleteComment, getComment, getComments } = require("../controller/comments");
const { protect, authorize } = require("../middleware/protect");

const router = express.Router();
// /api/v1/books
router.route('/').post(protect, authorize("admin", "operator", "user"), createComment).get(getComments);

router.route('/:id').put(protect, authorize("admin", "operator", "user"), updateComment).delete(protect, authorize("admin", "operator", "user"), deleteComment).get(getComment);
module.exports = router;