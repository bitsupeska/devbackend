const express = require("express");
const { protect, authorize } = require("../middleware/protect");
const {
  register,
  login,
  getUser,
  getUsers,
  createUser,
  updateUser,
  deleteUser,
  forgotPassword,
  resetPassword,
  logout,
} = require("../controller/users");
// const { getUserBooks } = require("../controller/books")
// const { getUserComments } = require("../controller/comments")
const router = express.Router(); //{ mergeParams: true }
//"api/v1/users"
router.route("/login").post(login);
router.route("/logout").get(logout);
router.route("/register").post(register);
router.route("/forgot-password").post(forgotPassword);
router.route("/reset-password").post(resetPassword);

// router.use(protect)
router.route("/").get(getUsers).post(createUser);
router.route("/:id").get(getUser).put(updateUser).delete(deleteUser);
// router.route('/:id/books').get(authorize("admin", "operator", "user"), getUserBooks)
// router.route('/:id/comments').get(authorize("admin", "operator", "user"), getUserComments)
module.exports = router;
